<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class attachFileMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $form;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$form)
    {
        $this->data = $data;
        $this->form = $form;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->from('quotations@osvpl.com', 'Quotations - OSVPL')
        return $this->from($this->data['from_mail'], $this->data['from_type'].'-'.$this->data['org_name'])
        ->replyTo('info@osvpl.com', 'Reply Guy')
        ->subject($this->data['subject'])->markdown('emails.attach_file')
        ->attach(public_path('Projects/'.$this->form['project_id'].'/'.$this->form['conversation_id'].'/'.$this->form['files']), [
            'as' => $this->form['files'],
            // 'mime' => 'application/pdf',
       ]);
        // return $this->view('view.name');
    }
}
