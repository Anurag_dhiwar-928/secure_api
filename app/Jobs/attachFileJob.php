<?php

namespace App\Jobs;

use App\Mail\attachFileMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use SebastianBergmann\Environment\Console;

class attachFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $data;
    public $form;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $form)
    {
        $this->data = $data;
        $this->form = $form;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Mail::to('anuragdhiwar928@gmail.com')
        Mail::to($this->data['toEmail'])
        ->cc(json_decode($this->data['ccEmail'],true))
        ->send(new attachFileMail($this->data,$this->form));
    }
}
