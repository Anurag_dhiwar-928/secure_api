<?php
namespace App\Helpers;

class Helpers{
    //ticket generator
    public static function IDGenerator($model, $trow, $length, $prefix){
        $data=$model::orderBy($trow,'desc')->first();
        if(!$data){
            $og_length=$length;
            $last_number ='';
            return $prefix.'-00001';
        }
        else{
            $code=substr($data->$trow,strlen($prefix)+1);
            $actial_last_number=($code/1)*1;
            $increament_last_number=$actial_last_number+1;
            $last_number_length=strlen($increament_last_number);
            $og_length=$length-$last_number_length;
            $last_number=$increament_last_number;
        }
        $zeros="";
        for($i=0;$i<$og_length;$i++){
            $zeros.="0";
        }
        return $prefix.'-'.$zeros.$last_number;
    }
    //Enquiry Number generator
    public static function EnqGenerator($model, $trow, $length, $prefix){
        $data=$model::orderBy('appointment_sno','desc')->first();
        if(!$data){
            $og_length=$length;
            $last_number ='';
            return $prefix.'-0001';
        }
        else{
            $code=substr($data->$trow,strlen($prefix)+1);
            $actial_last_number=($code/1)*1;
            $increament_last_number=$actial_last_number+1;
            $last_number_length=strlen($increament_last_number);
            $og_length=$length-$last_number_length;
            $last_number=$increament_last_number;
        }
        $zeros="";
        for($i=0;$i<$og_length;$i++){
            $zeros.="0";
        }
        return $prefix.'-'.$zeros.$last_number;
    }

    //emp_no generator for our employee
    public static function empNOGenerator($model, $trow, $length, $prefix){
        $data=$model::orderBy('emp_no','desc')->first();
        if(!$data){
            $og_length=$length;
            $last_number ='';
        }
        else{
            $code=substr($data->$trow,strlen($prefix)+1);
            $actial_last_number=($code/1)*1;
            $increament_last_number=$actial_last_number+1;
            $last_number_length=strlen($increament_last_number);
            $og_length=$length-$last_number_length;
            $last_number=$increament_last_number;
        }
        $zeros="";
        for($i=0;$i<$og_length;$i++){
            $zeros.="0";
        }
        return $prefix.'-'.$zeros.$last_number;
    }
}

?>