<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // use UuidTrait;
    protected $table     = "users";
    // protected $primaryKey = 'user_id';
    // public $incrementing = false;
    protected $fillable  = [
        'user_id',
        'name',
        'mobile',
        'password',
        'user_type',
        'delete_status',
        'created_by'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
        'created_at',
        'updated_at', 
        'deleted_at'
    ];

}
