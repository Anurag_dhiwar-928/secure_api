<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankFinance extends Model
{
    use HasFactory, SoftDeletes;

    protected $table     = "bank_finances";
    // protected $primaryKey = 'user_id';
    // public $incrementing = false;
    protected $fillable  = [
        'bank_finance_id', 'reg_no', 'date', 'customer_name', 'contact_no', 'address', 'total_amount', 'advanced_amount', 'down_payment', 'finance_amount', 'payout', 'bank_name', 'remarks', 'active', 'created_by', 'created_at', 'updated_at'
    ];
    protected $hidden = ['deleted_at'];
    protected $dates = ['deleted_at'];
}
