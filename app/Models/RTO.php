<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RTO extends Model
{
    use HasFactory, SoftDeletes;

    protected $table     = "r_t_o_s";
    // protected $primaryKey = 'user_id';
    // public $incrementing = false;
    protected $fillable  = [
        'r_t_o_id', 'reg_no', 'date', 'customer_name', 'contact_no', 'model', 'year', 'transfer_amount', 'bank_transfer', 'self_transfer', 'rto_agent_name', 'remarks', 'active', 'created_by', 'created_at', 'updated_at'
    ];
    protected $hidden = ['deleted_at'];
    protected $dates = ['deleted_at'];
}
