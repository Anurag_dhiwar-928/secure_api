<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    use HasFactory, SoftDeletes;

    protected $table     = "banners";
    // protected $primaryKey = 'user_id';
    // public $incrementing = false;
    protected $fillable  = [
        'banner_id', 'priority', 'banner_type', 'img', 'remarks', 'active', 'created_by', 'created_at', 'updated_at'
    ];
    protected $hidden = ['deleted_at'];
    protected $dates = ['deleted_at'];
}
