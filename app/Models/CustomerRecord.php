<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerRecord extends Model
{
    use HasFactory, SoftDeletes;

    protected $table     = "customer_records";
    // protected $primaryKey = 'user_id';
    // public $incrementing = false;
    protected $fillable  = [
        'customer_record_id', 'reg_no', 'date', 'customer_name', 'contact_no', 'address', 'car_req', 'year', 'amount', 'remarks', 'active', 'created_by', 'created_at', 'updated_at'
    ];
    protected $hidden = ['deleted_at'];
    protected $dates = ['deleted_at'];
}
