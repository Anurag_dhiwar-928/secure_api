<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OurFinanceFacility extends Model
{
    use HasFactory, SoftDeletes;

    protected $table     = "our_finance_facilities";
    // protected $primaryKey = 'user_id';
    // public $incrementing = false;
    protected $fillable  = [
        'our_finance_id', 'name', 'emp_name', 'emp_contact', 'emp_wa_contact', 'designation', 'img_url', 'remarks', 'active', 'created_by', 'created_at', 'updated_at'
    ];
    protected $hidden = ['deleted_at'];
    protected $dates = ['deleted_at'];
}
