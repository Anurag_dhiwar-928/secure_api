<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseRecord extends Model
{
    use HasFactory, SoftDeletes;

    protected $table     = "purchase_records";
    // protected $primaryKey = 'user_id';
    // public $incrementing = false;
    protected $fillable  = [
        'purchase_record_id', 'reg_no', 'date', 'customer_name', 'contact_no', 'address', 'total_deal', 'cash_amount', 'transfer_amount', 'bank_finance', 'utr_no', 'transfer_ac_detail', 'bank_name', 'remarks', 'active', 'created_by', 'created_at', 'updated_at'
    ];
    protected $hidden = ['deleted_at'];
    protected $dates = ['deleted_at'];
}
