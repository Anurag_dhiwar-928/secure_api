<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SellCar extends Model
{
    use HasFactory, SoftDeletes;

    protected $table     = "sell_cars";
    // protected $primaryKey = 'user_id';
    // public $incrementing = false;
    protected $fillable  = [
        'sell_car_id', 'registration_no', 'brand', 'reg_year', 'model', 'variant', 'fuel', 'ownership', 'km_driven', 'sell_price', 'user_name', 'mobile', 'address', 'is_whatsapp', 'status','remarks', 'active', 'created_by', 'created_at', 'updated_at'
    ];
    protected $hidden = ['deleted_at'];
    protected $dates = ['deleted_at'];
}