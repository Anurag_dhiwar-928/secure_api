<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OurTeam extends Model
{
    use HasFactory, SoftDeletes;

    protected $table     = "our_teams";
    // protected $primaryKey = 'user_id';
    // public $incrementing = false;
    protected $fillable  = [
        'our_team_id', 'name', 'contact_no', 'designation', 'photo', 'remarks', 'created_by', 'created_at', 'updated_at'
    ];
    protected $hidden = ['deleted_at'];
    protected $dates = ['deleted_at'];
}
