<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model
{
    use HasFactory, SoftDeletes;

    protected $table     = "cars";
    // protected $primaryKey = 'user_id';
    // public $incrementing = false;
    protected $fillable  = [
        'car_id', 'brand', 'name', 'model', 'kms', 'owner', 'insurance', 'price', 'commission', 'discount', 'fuel', 'owner_comment', 'status', 'is_parked', 'sold_date', 'sold_price', 'location', 'sold_location', 'photos', 'sold_photos', 'remarks','created_by', 'created_at', 'updated_at'
    ];
    protected $casts = [
        'photos' => 'array',
    ];
    protected $hidden = ['deleted_at'];
    protected $dates = ['deleted_at'];
}
