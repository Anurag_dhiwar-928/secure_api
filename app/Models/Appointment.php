<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model
{
    use HasFactory, SoftDeletes;

    protected $table     = "appointments";
    // protected $primaryKey = 'user_id';
    // public $incrementing = false;
    protected $fillable  = [
        'appointment_id', 'enquiry_no', 'name', 'mobile', 'email', 'address', 'car_id', 'status', 'is_viewed', 'remarks', 'created_by', 'created_at', 'updated_at'
    ];
    protected $hidden = ['deleted_at'];
    protected $dates = ['deleted_at'];
}
