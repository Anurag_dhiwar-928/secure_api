<?php

namespace App\Http\Traits;

use Webpatser\Uuid\Uuid;

trait UuidTrait
{

     /**
     * Boot function from Laravel.
     */
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Uuid::generate(4)->string;
                // $model->{$model->created_by} = $request->user()->user_id;
            }
        });
    }

 }