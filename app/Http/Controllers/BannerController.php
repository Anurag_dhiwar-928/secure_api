<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Webpatser\Uuid\Uuid;

class BannerController extends Controller
{
    //add/update Banners 
    public function post(Request $request)
    {
        $banner_id = $request->input('banner_id');
        $operation = $request->input('operation');
        try {
            $request->validate([
                'banner_type' => 'required',
                'img'         => 'required',
                'priority'    => 'required'
            ]);
            $banner_type = $request->get('banner_type');
            $img         = $request->get('img');
            $remarks     = $request->get('remarks');
            $priority    = $request->get('priority');
            $created_by  = $request->user()->user_id;
            $form = array(
                'banner_type' => $banner_type,
                'img'         => $img,
                'remarks'     => $remarks,
                'priority'    => $priority
            );
            if ($operation === 'update') {
                Banner::where('banner_id', $banner_id)->update($form);
                return response()->json(['message' => 'Banner updated successfully', 'success' => true], 201);
            } else {
                $form['banner_id'] = Uuid::generate(4)->string;
                $form['created_by'] = $created_by;
                Banner::create($form);
                return response()->json(['message' => 'Banner uploaded successfully', 'success' => true], 201);
            }
        } catch (Exception $e) {
            if (isset($e->errorInfo[2])) {
                return Response()->json(['message' => $e->errorInfo[2], 'error_code' => $e->errorInfo[1], 'success' => false], 500);
            } else {
                return Response()->json(['message' => $e->getMessage(), 'success' => false], 500);
            }
        }
    }

    //get Banners
    public function get(Request $request)
    {
        try {
            $banner_id = $request->input('banner_id');
            $banner_type = $request->input('banner_type');
            $operation = $request->input('operation');
            if ($operation == 'delete') {
                Banner::where('banner_id', $banner_id)->delete();
                $data = 'Banner deleted successfully!';
            }
            else if ($operation == 'log') {
                $form = array(
                    'active' => $request->input('active')
                );
                Banner::where('banner_id', $banner_id)->update($form);
                $data = 'Changed active log successfully!';
            }
            else if ($operation == 'getById') {
                $data = Banner::where([['banner_id', $banner_id], ['active', 1]])->first();
            }
            else if ($operation == 'website') {
                $data = Banner::where([['banner_type', $banner_type], ['active', 1]])->orderBy('priority', 'DESC')->get();
            }
            else {
                $data = Banner::orderBy('banner_sno', 'DESC')->get();
            }
            return response()->json(['data' => $data, 'success' => true], 200);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
        }
    }
}
