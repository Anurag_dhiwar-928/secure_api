<?php

namespace App\Http\Controllers;

use App\Models\BankFinance;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Webpatser\Uuid\Uuid;

class BankFinanceController extends Controller
{
    //add/update Bank Finance Record 
    public function post(Request $request)
    {
        $bank_finance_id = $request->input('bank_finance_id');
        $function = $request->input('function');

        try {
            $request->validate([
                'reg_no'          => 'required',
                'date'            => 'required',
                'customer_name'   => 'required',
                'contact_no'      => 'required',
                'address'         => 'required',
                'total_amount'    => 'required',
                'advanced_amount' => 'required',
                'down_payment'    => 'required',
                'finance_amount'  => 'required',
                'payout'          => 'required',
                'bank_name'       => 'required',
            ]);
            $reg_no          = $request->get('reg_no');
            $date            = $request->get('date');
            $customer_name   = $request->get('customer_name');
            $contact_no      = $request->get('contact_no');
            $address         = $request->get('address');
            $total_amount    = $request->get('total_amount');
            $advanced_amount = $request->get('advanced_amount');
            $down_payment    = $request->get('down_payment');
            $finance_amount  = $request->get('finance_amount');
            $payout          = $request->get('payout');
            $bank_name       = $request->get('bank_name');
            $remarks         = $request->get('remarks');
            $created_by      = $request->user()->user_id;
            $form = array(
                'reg_no'          => $reg_no,
                'date'            => $date,
                'customer_name'   => $customer_name,
                'contact_no'      => $contact_no,
                'address'         => $address,
                'total_amount'    => $total_amount,
                'advanced_amount' => $advanced_amount,
                'down_payment'    => $down_payment,
                'finance_amount'  => $finance_amount,
                'payout'          => $payout,
                'bank_name'       => $bank_name,
                'remarks'         => $remarks,
            );
            if ($function === 'update') {
                BankFinance::where('bank_finance_id', $bank_finance_id)->update($form);
                return response()->json(['message' => 'Bank Finance Record updated successfully', 'success' => true], 201);
            } else {
                $form['bank_finance_id'] = Uuid::generate(4)->string;
                $form['created_by'] = $created_by;
                BankFinance::create($form);
                return response()->json(['message' => 'Bank Finance Record uploaded successfully', 'success' => true], 201);
            }
        } catch (Exception $e) {
            if (isset($e->errorInfo[2])) {
                return Response()->json(['message' => $e->errorInfo[2], 'error_code' => $e->errorInfo[1], 'success' => false], 500);
            } else {
                return Response()->json(['message' => $e->getMessage(), 'success' => false], 500);
            }
        }
    }

      //get Bank Finance Record 
      public function get(Request $request)
      {
          try {
              $bank_finance_id = $request->input('bank_finance_id');
              if ($bank_finance_id) {
                  $data = BankFinance::where('bank_finance_id', $bank_finance_id)->first();
              } else {
                  $data = BankFinance::orderBy('bank_finance_sno', 'DESC')->get();;
              }
              return response()->json(['data' => $data, 'success' => true], 200);
          } catch (Exception $e) {
              return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
          }
      }
}
