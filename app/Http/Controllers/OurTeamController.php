<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\BankFinance;
use App\Models\Car;
use App\Models\CustomerRecord;
use App\Models\OurFinanceFacility;
use App\Models\OurTeam;
use App\Models\PurchaseRecord;
use App\Models\RTO;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Webpatser\Uuid\Uuid;

class OurTeamController extends Controller
{
    //add our team  
    public function post(Request $request)
    {
        try {
            if ($request->input('function') == 'update') {
                $request->validate([
                    'name'        => 'required',
                    'contact_no'  => 'required',
                    'designation' => 'required'
                ]);
                $our_team_id = $request->input('our_team_id');
                $name        = $request->get('name');
                $contact_no  = $request->get('contact_no');
                $designation = $request->get('designation');
                $photo       = $request->get('photo');
                $remarks     = $request->get('remarks');
                if ($photo) {
                    if (!File::exists(public_path('images/our_team'))) {
                        File::makeDirectory(public_path('images/our_team'));
                    }
                    $folderPath     = 'images/our_team/';
                    $image_parts    = explode(";base64,", $photo);
                    $image_type_aux = explode("image/", $image_parts[0]);
                    $image_type     = $image_type_aux[1];
                    $image_base64   = base64_decode($image_parts[1]);
                    $file           = $folderPath . $our_team_id . '.' . $image_type;
                    file_put_contents($file, $image_base64);
                    $photo          = $our_team_id . '.' . $image_type;
                }
                $form = array(
                    'name'        => $name,
                    'contact_no'  => $contact_no,
                    'designation' => $designation,
                    // 'photo'       => $photo,
                    'remarks'     => $remarks
                );
                if ($photo) {
                    $form['photo'] = $photo;
                }
                OurTeam::where('our_team_id', $our_team_id)->update($form);
                return response()->json(['message' => 'Our Team updated successfully', 'success' => true], 200);
            } else {
                $request->validate([
                    'name'        => 'required',
                    'contact_no'  => 'required',
                    'designation' => 'required',
                    'photo'       => 'required'
                ]);
                $our_team_id   = Uuid::generate(4)->string;
                $name        = $request->get('name');
                $contact_no  = $request->get('contact_no');
                $designation = $request->get('designation');
                $photo       = $request->get('photo');
                $remarks       = $request->get('remarks');
                $created_by = $request->user()->user_id;
                if (!File::exists(public_path('images/our_team'))) {
                    File::makeDirectory(public_path('images/our_team'));
                }
                $folderPath     = 'images/our_team/';
                $image_parts    = explode(";base64,", $photo);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type     = $image_type_aux[1];
                $image_base64   = base64_decode($image_parts[1]);
                $file           = $folderPath . $our_team_id . '.' . $image_type;
                file_put_contents($file, $image_base64);
                $form = array(
                    'our_team_id' => $our_team_id,
                    'name'        => $name,
                    'contact_no'  => $contact_no,
                    'designation' => $designation,
                    'photo'       => $our_team_id . '.' . $image_type,
                    'remarks'     => $remarks,
                    'created_by'  => $created_by
                );
                OurTeam::create($form);
                return response()->json(['message' => 'Our Team uploaded successfully', 'success' => true], 201);
            }
        } catch (Exception $e) {
            if (isset($e->errorInfo[2])) {
                return Response()->json(['message' => $e->errorInfo[2], 'error_code' => $e->errorInfo[1], 'success' => false], 500);
            } else {
                return Response()->json(['message' => $e->getMessage(), 'success' => false], 500);
            }
        }
    }

    //get our team
    public function get(Request $request)
    {
        try {
            $our_team_id = $request->input('our_team_id');
            if ($our_team_id) {
                $data = OurTeam::where('our_team_id', $our_team_id)->first();
            } else {
                $data = OurTeam::orderBy('our_team_sno', 'DESC')->get();;
            }
            return response()->json(['data' => $data, 'success' => true], 200);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
        }
    }

     //add our finance facility  
     public function postFinance(Request $request)
     {
         try {
            $request->validate([
                'name'           => 'required',
                'emp_name'       => 'required',
                'emp_contact'    => 'required'
            ]);
            $our_finance_id = $request->input('our_finance_id');
            $operation = $request->input('operation');

            $name           = $request->get('name');
            $emp_name       = $request->get('emp_name');
            $emp_contact    = $request->get('emp_contact');
            $emp_wa_contact = $request->get('emp_wa_contact');
            $designation    = $request->get('designation');
            $img_url        = $request->get('img_url');
            $remarks        = $request->get('remarks');
            $created_by = $request->user()->user_id;
            $form = array(
                'name'           => $name,
                'emp_name'       => $emp_name,
                'emp_contact'    => $emp_contact,
                'emp_wa_contact' => $emp_wa_contact,
                'designation'    => $designation,
                'img_url'        => $img_url,
                'remarks'        => $remarks
            );
             if ($operation == 'update') {
                 OurFinanceFacility::where('our_finance_id', $our_finance_id)->update($form);
                 return response()->json(['message' => 'Our Finance facility updated successfully', 'success' => true], 200);
             } else {
                 $form['our_finance_id'] = Uuid::generate(4)->string;
                 $form['created_by'] = $created_by;
                 OurFinanceFacility::create($form);
                 return response()->json(['message' => 'Our Finance facility added successfully', 'success' => true], 201);
             }
         } catch (Exception $e) {
             if (isset($e->errorInfo[2])) {
                 return Response()->json(['message' => $e->errorInfo[2], 'error_code' => $e->errorInfo[1], 'success' => false], 500);
             } else {
                 return Response()->json(['message' => $e->getMessage(), 'success' => false], 500);
             }
         }
     }

     //get our finance facility
    public function getFinance(Request $request)
    {
        try {
            $our_finance_id = $request->input('our_finance_id');
            $operation = $request->input('operation');
            if ($operation == 'delete') {
                OurFinanceFacility::where('our_finance_id', $our_finance_id)->delete();
                $data = 'Financial record deleted successfully!';
            }
            else if ($operation == 'log') {
                $form = array(
                    'active' => $request->input('active')
                );
                OurFinanceFacility::where('our_finance_id', $our_finance_id)->update($form);
                $data = 'Changed active log successfully!';
            }
            else if ($operation == 'getById') {
                $data = OurFinanceFacility::where('our_finance_id', $our_finance_id)->first();
            }
            else if ($operation == 'website') {
                $data = OurFinanceFacility::where('active', 1)->orderBy('our_finance_sno', 'ASC')->get();
            }
            else {
                $data = OurFinanceFacility::orderBy('our_finance_sno', 'DESC')->get();
            }
            return response()->json(['data' => $data, 'success' => true], 200);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
        }
    }

     //get count data for dashboard
     public function getCountForDashboard(Request $request)
     {
         try {
                 $our_financial = OurFinanceFacility::count();
                 $car_sale = Car::where('status', 'Sale')->count();
                 $car_sold = Car::where('status', 'Sold')->count();
                 $appointment = Appointment::count();
                 $our_team = OurTeam::count();
                 $bank_finance = BankFinance::count();
                 $customers = CustomerRecord::count();
                 $RTO = RTO::count();
                 $purchase_record = PurchaseRecord::count();
             return response()->json([
                'our_financial' => $our_financial, 
                'car_sale' => $car_sale, 
                'car_sold' => $car_sold, 
                'appointment' => $appointment, 
                'our_team' => $our_team,
                'bank_finance' => $bank_finance,
                'customers' => $customers,
                'rto' => $RTO,
                'purchase_record' => $purchase_record,
                'success' => true], 200);
         } catch (Exception $e) {
             return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
         }
     }
 
}
