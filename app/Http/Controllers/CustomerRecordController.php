<?php

namespace App\Http\Controllers;

use App\Models\CustomerRecord;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Webpatser\Uuid\Uuid;

class CustomerRecordController extends Controller
{
    //add/update Customer Record 
    public function post(Request $request)
    {
        $customer_record_id = $request->input('customer_record_id');
        $function = $request->input('function');

        try {
            $request->validate([
                'date'          => 'required',
                'customer_name' => 'required',
                'contact_no'    => 'required',
                'address'       => 'required',
                'car_req'       => 'required',
                'year'          => 'required',
                'amount'        => 'required'
            ]);
            $date            = $request->get('date');
            $customer_name   = $request->get('customer_name');
            $contact_no      = $request->get('contact_no');
            $address         = $request->get('address');
            $car_req         = $request->get('car_req');
            $year            = $request->get('year');
            $amount          = $request->get('amount');
            $remarks         = $request->get('remarks');
            $created_by      = $request->user()->user_id;
            $form = array(
                'date'          => $date,
                'customer_name' => $customer_name,
                'contact_no'    => $contact_no,
                'address'       => $address,
                'car_req'       => $car_req,
                'year'          => $year,
                'amount'        => $amount,
                'remarks'       => $remarks,
            );
            if ($function === 'update') {
                CustomerRecord::where('customer_record_id', $customer_record_id)->update($form);
                return response()->json(['message' => 'Customer Record updated successfully', 'success' => true], 201);
            } else {
                $form['customer_record_id'] = Uuid::generate(4)->string;
                $form['created_by'] = $created_by;
                CustomerRecord::create($form);
                return response()->json(['message' => 'Customer Record uploaded successfully', 'success' => true], 201);
            }
        } catch (Exception $e) {
            if (isset($e->errorInfo[2])) {
                return Response()->json(['message' => $e->errorInfo[2], 'error_code' => $e->errorInfo[1], 'success' => false], 500);
            } else {
                return Response()->json(['message' => $e->getMessage(), 'success' => false], 500);
            }
        }
    }

    //get Customer Record 
    public function get(Request $request)
    {
        try {
            $customer_record_id = $request->input('customer_record_id');
            if ($customer_record_id) {
                $data = CustomerRecord::where('customer_record_id', $customer_record_id)->first();
            } else {
                $data = CustomerRecord::orderBy('customer_record_sno', 'DESC')->get();;
            }
            return response()->json(['data' => $data, 'success' => true], 200);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
        }
    }
}
