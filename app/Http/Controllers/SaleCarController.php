<?php

namespace App\Http\Controllers;

use App\Models\SellCar;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Webpatser\Uuid\Uuid;

class SaleCarController extends Controller
{
    //add cars for sale by website 
    public function post(Request $request)
    {
        $sell_car_id = $request->input('sell_car_id');
        $function = $request->input('function');
        try {
            $request->validate([
                'registration_no' => 'required',
                'customer_name'   => 'required',
                'contact_no'      => 'required',
                'model'           => 'required',
                'reg_year'            => 'required'
            ]);
            $registration_no = $request->get('registration_no');
            $brand           = $request->get('brand');
            $reg_year        = $request->get('reg_year');
            $model           = $request->get('model');
            $variant         = $request->get('variant');
            $fuel            = $request->get('fuel');
            $ownership       = $request->get('ownership');
            $km_driven       = $request->get('km_driven');
            $sell_price      = $request->get('sell_price');
            $user_name       = $request->get('customer_name');
            $mobile          = $request->get('contact_no');
            $address         = $request->get('address');
            $is_whatsapp     = $request->get('is_whatsapp');
            $status          = 'Pending';
            $remarks         = $request->get('remarks');
            $created_by      = request()->ip();
            $form = array(
                'registration_no' => $registration_no,
                'brand'           => $brand,
                'reg_year'        => $reg_year,
                'model'           => $model,
                'variant'         => $variant,
                'ownership'       => $ownership,
                'fuel'            => $fuel,
                'km_driven'       => $km_driven,
                'sell_price'      => $sell_price,
                'user_name'       => $user_name,
                'mobile'          => $mobile,
                'address'         => $address,
                'is_whatsapp'     => $is_whatsapp,
                'status'          => $status,
                'remarks'         => $remarks
            );
            if ($function === 'update') {
                SellCar::where('sell_car_id', $sell_car_id)->update($form);
                return response()->json(['message' => 'Record updated successfully', 'success' => true], 200);
            } else {
                $form['sell_car_id'] = Uuid::generate(4)->string;
                $form['created_by']  = $created_by;
                SellCar::create($form);
                return response()->json(['message' => 'Record uploaded successfully', 'success' => true], 201);
            }
        } catch (Exception $e) {
            if (isset($e->errorInfo[2])) {
                return Response()->json(['message' => $e->errorInfo[2], 'error_code' => $e->errorInfo[1], 'success' => false], 500);
            } else {
                return Response()->json(['message' => $e->getMessage(), 'success' => false], 500);
            }
        }
    }

      //get cars for sale by website
      public function get(Request $request)
      {
          try {
              $sell_car_id = $request->input('sell_car_id');
              $operation = $request->input('operation');
              if ($operation == 'delete') {
                  SellCar::where('sell_car_id', $sell_car_id)->delete();
                  $data = 'Record deleted successfully!';
              }
              else if ($operation == 'log') {
                  $form = array(
                      'active' => $request->input('active')
                  );
                  SellCar::where('sell_car_id', $sell_car_id)->update($form);
                  $data = 'Changed active log successfully!';
              }
              else if ($operation == 'getById') {
                  $data = SellCar::where('sell_car_id', $sell_car_id)->first();
              }
              else if ($operation == 'website') {
                  $data = SellCar::where('active', 1)->orderBy('sell_car_sno', 'DESC')->get();
              }
              else {
                  $data = SellCar::orderBy('sell_car_sno', 'DESC')->get();
              }
              return response()->json(['data' => $data, 'success' => true], 200);
          } catch (Exception $e) {
              return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
          }
      }
}
