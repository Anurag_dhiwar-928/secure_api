<?php

namespace App\Http\Controllers;

use App\Helpers\Helpers;
use App\Models\Car;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Webpatser\Uuid\Uuid;

class CarController extends Controller
{
    //add car 
    public function create(Request $request)
    {
        try {
            if ($request->input('function') == 'update') {
                $request->validate([
                    'sold_date' => 'required',
                    'sold_price'  => 'required',
                    'sold_location' => 'required',
                    'sold_photos'   => 'required'
                ]);
                $car_id        = $request->input('car_id');
                $status        = 'Sold';
                // $is_parked     = $request->get('is_parked');
                $sold_date     = $request->get('sold_date');
                $sold_price    = $request->get('sold_price');
                $sold_location = $request->get('sold_location');
                $sold_photos   = $request->get('sold_photos');
                $i = 0;
                $photo_arr = [];
                foreach ($sold_photos as $photo) {
                    $i = $i + 1;
                    if (!File::exists(public_path('images/cars/sold/' . $car_id))) {
                        File::makeDirectory(public_path('images/cars/sold/' . $car_id));
                    }
                    $folderPath = 'images/cars/sold/' . $car_id . '/';
                    $image_parts = explode(";base64,", $photo);
                    $image_type_aux = explode("image/", $image_parts[0]);
                    $image_type = $image_type_aux[1];
                    $image_base64 = base64_decode($image_parts[1]);
                    $file = $folderPath . $i . '.' . $image_type;
                    file_put_contents($file, $image_base64);
                    array_push($photo_arr, $i . '.' . $image_type);
                }
                $form = array(
                    'status'        => $status,
                    'sold_date'     => $sold_date,
                    'sold_price'    => $sold_price,
                    'sold_location' => $sold_location,
                    'sold_photos'   => $photo_arr
                );
                Car::where('car_id', $car_id)->update($form);
                return response()->json(['message' => 'Car sold successfully', 'success' => true], 200);
            } else {
                $request->validate([
                    'brand' => 'required',
                    'name'  => 'required',
                    'model' => 'required',
                    'kms'   => 'required'
                ]);
                $car_id        = Uuid::generate(4)->string;
                $brand         = $request->get('brand');
                $name          = $request->get('name');
                $model         = $request->get('model');
                $kms           = $request->get('kms');
                $owner         = $request->get('owner');
                $insurance     = $request->get('insurance');
                $price         = $request->get('price');
                $commission    = $request->get('commission');
                $discount      = $request->get('discount');
                $fuel          = $request->get('fuel');
                $owner_comment = $request->get('owner_comment');
                $location      = $request->get('location');
                $photos        = $request->get('photos');
                $remarks       = $request->get('remarks');
                // $is_parked     = $request->get('is_parked');
                $created_by = $request->user()->user_id;
                $i = 0;
                $photo_arr = [];
                foreach ($photos as $photo) {
                    $i = $i + 1;
                    if (!File::exists(public_path('images/cars/sale/' . $car_id))) {
                        File::makeDirectory(public_path('images/cars/sale/' . $car_id));
                    }
                    $folderPath = 'images/cars/sale/' . $car_id . '/';
                    $image_parts = explode(";base64,", $photo);
                    $image_type_aux = explode("image/", $image_parts[0]);
                    $image_type = $image_type_aux[1];
                    $image_base64 = base64_decode($image_parts[1]);
                    $file = $folderPath . $i . '.' . $image_type;
                    file_put_contents($file, $image_base64);
                    array_push($photo_arr, $i . '.' . $image_type);
                }
                $form = array(
                    'car_id'        => $car_id,
                    'brand'         => $brand,
                    'name'          => $name,
                    'model'         => $model,
                    'kms'           => $kms,
                    'owner'         => $owner,
                    'insurance'     => $insurance,
                    'price'         => $price,
                    'commission'    => $commission,
                    'discount'      => $discount,
                    'fuel'          => $fuel,
                    'owner_comment' => $owner_comment,
                    'location'      => $location,
                    'photos'        => $photo_arr,
                    'remarks'       => $remarks,
                    'created_by'    => $created_by
                );
                Car::create($form);
                return response()->json(['message' => 'Car uploaded successfully', 'success' => true, 'photo_arr' => $request->user()], 201);
            }
        } catch (Exception $e) {
            if (isset($e->errorInfo[2])) {
                return Response()->json(['message' => $e->errorInfo[2], 'error_code' => $e->errorInfo[1], 'success' => false, 'photo_arr' => $request->user()], 500);
            } else {
                return Response()->json(['message' => $e->getMessage(), 'success' => false, 'photo_arr' => $request->user()], 500);
            }
        }
    }

    //get cars 
    public function getAll(Request $request)
    {
        try {
            $car_id = $request->input('id');
            if ($car_id) {
                $data = Car::where('car_id', $car_id)->first();
            } else {
                $data = Car::orderBy('car_sno', 'DESC')->get();
            }
            return response()->json(['data' => $data, 'success' => true], 200);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
        }
    }
}
