<?php

namespace App\Http\Controllers;

use App\Models\RolePrmsnPrmsnGrp;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Webpatser\Uuid\Uuid;

class UserController extends Controller
{
     /**
     * Set Authentication
     */
    public function authentication(Request $request)
    {
        try {
            // return Response()->json(['data' => $request->all()]);
           $request->validate([
                'mobile'   => 'required',
                'password' => 'required'
            ]);


            $mobile   = $request->get('mobile');
            $password = $request->get('password');

            
            $user = User::where([['mobile', $mobile], ['password', $password]])->first();
            
            $response = array();

            if ($user) {
                $data['token']       = $user->createToken('passport')->accessToken;
                $data['user']        = $user;
                $response['data']    = array('data' => $data);
                $response['status']  = 200;
            } else {
                $response['data']    = array('message' => 'Invalid mobile and password.');
                $response['status']  = 404;
            }

            return Response()->json($response['data'],  $response['status']);
        } catch (Exception $e) {
            return Response()->json(['message' => $e->getMessage(),'data' => $request->all()],  500);
        }
    }

    //get user detail by id
    public function getUsers(){
        try {
            $data = User::all();
            return response()->json(['data' => $data, 'success' => true], 200);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
        }
    }

    //get user detail by id
    public function getUsersById($user_id){
        try {
            $data = User::where('user_id', $user_id)->first();
            return response()->json(['data' => $data, 'success' => true], 200);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
        }
    }

    //create user 
    public function createUsers(Request $request){
        try {
           $request->validate([
               'name'     => 'required',
               'mobile'   => 'required',
               'password' => 'required'
           ]);
           $name       = $request->get('name');
           $mobile     = $request->get('mobile');
           $password   = $request->get('password');
           $created_by = $request->user()->user_id;

           $user_form = array(
               'user_id'    => Uuid::generate(4)->string,
               'name'       => $name,
               'mobile'     => $mobile,
               'password'   => $password,
               'created_by' => $created_by
           );
           User::create($user_form);
            return response()->json(['message' => 'User created successfully', 'success' => true], 201);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
        }
    }

    //update user 
    public function updateUsers(Request $request){
        try {
           $request->validate([
               'user_id'   => 'required',
               'name'      => 'required',
               'mobile'    => 'required',
               'password'  => 'required',
               'user_type' => 'required'
           ]);
           $user_id    = $request->get('user_id');
           $name       = $request->get('name');
           $mobile     = $request->get('mobile');
           $password   = $request->get('password');
           $user_type  = $request->get('user_type');
           $user_form  = array(
               'name'      => $name,
               'mobile'    => $mobile,
               'password'  => $password,
               'user_type' => $user_type
           );
           User::where('user_id', $user_id)->update($user_form);
            return response()->json(['message' => 'User updated successfully', 'success' => true], 201);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
        }
    }

    //delete user
    public function deleteUsers($user_id) {
        $user_check = User::where('user_id', $user_id)->count();
        if ($user_check == 0) {
            return response()->json(['message' => 'Id does not exist!', 'success' => false], 404);
        }
        else{
            User::where('user_id', $user_id)->delete();
            return response()->json(['message' => 'Deleted successfully', 'success' => true], 200);
        }
    }

}
