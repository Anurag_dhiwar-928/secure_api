<?php

namespace App\Http\Controllers;

use App\Helpers\Helpers;
use App\Models\Appointment;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Webpatser\Uuid\Uuid;

class AppointmentController extends Controller
{
    //create ticket 
    public function create(Request $request)
    {
        try {
            $request->validate([
                'name'    => 'required',
                'mobile'  => 'required',
                'address' => 'required'
            ]);
            $appointment_id = Uuid::generate(4)->string;
            $enq_no         = Helpers::EnqGenerator(new Appointment, 'enquiry_no', 4, 'C4U-ENQ');
            $name           = $request->get('name');
            $mobile         = $request->get('mobile');
            $address        = $request->get('address');
            $remarks        = $request->get('remarks');
            $created_by     = request()->ip();
            if($request->get('car_id')) {
                $car_id = $request->get('car_id');
            } else {
                $car_id = '';
            }
            $form = array(
                'appointment_id' => $appointment_id,
                'enquiry_no'     => $enq_no,
                'car_id'         => $car_id,
                'name'           => $name,
                'mobile'         => $mobile,
                'address'        => $address,
                'remarks'        => $remarks,
                'created_by'     => $created_by
            );
            Appointment::create($form);
            return response()->json(['message' => 'Registration successfully', 'success' => true], 201);
        } catch (Exception $e) {
            if (isset($e->errorInfo[2])) {
                return Response()->json(['message' => $e->errorInfo[2], 'error_code' => $e->errorInfo[1], 'success' => false], 500);
            } else {
                return Response()->json(['message' => $e->getMessage(), 'success' => false], 500);
            }
        }
    }

    //get ticket 
    public function getAll()
    {
        try {
            $data = Appointment::get();
            return response()->json(['data' => $data, 'success' => true], 200);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
        }
    }
}
