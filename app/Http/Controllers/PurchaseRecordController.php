<?php

namespace App\Http\Controllers;

use App\Models\PurchaseRecord;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Webpatser\Uuid\Uuid;

class PurchaseRecordController extends Controller
{
     //add/update Customer Record 
     public function post(Request $request)
     {
         $purchase_record_id = $request->input('purchase_record_id');
         $function = $request->input('function');
 
         try {
             $request->validate([
                 'reg_no'        => 'required',
                 'date'          => 'required',
                 'customer_name' => 'required',
                 'contact_no'    => 'required',
                 'address'       => 'required',
                 'total_deal'    => 'required',
             ]);
             $reg_no             = $request->get('reg_no');
             $date               = $request->get('date');
             $customer_name      = $request->get('customer_name');
             $contact_no         = $request->get('contact_no');
             $address            = $request->get('address');
             $total_deal         = $request->get('total_deal');
             $cash_amount        = $request->get('cash_amount');
             $transfer_amount    = $request->get('transfer_amount');
             $bank_finance       = $request->get('bank_finance');
             $utr_no             = $request->get('utr_no');
             $transfer_ac_detail = $request->get('transfer_ac_detail');
             $bank_name          = $request->get('bank_name');
             $remarks            = $request->get('remarks');
             $created_by         = $request->user()->user_id;
             $form = array(
                 'reg_no'             => $reg_no,
                 'date'               => $date,
                 'customer_name'      => $customer_name,
                 'contact_no'         => $contact_no,
                 'address'            => $address,
                 'total_deal'         => $total_deal,
                 'cash_amount'        => $cash_amount,
                 'transfer_amount'    => $transfer_amount,
                 'bank_finance'       => $bank_finance,
                 'utr_no'             => $utr_no,
                 'transfer_ac_detail' => $transfer_ac_detail,
                 'bank_name'          => $bank_name,
                 'remarks'            => $remarks,
             );
             if ($function === 'update') {
                 PurchaseRecord::where('purchase_record_id', $purchase_record_id)->update($form);
                 return response()->json(['message' => 'Purchase Record updated successfully', 'success' => true], 201);
             } else {
                 $form['purchase_record_id'] = Uuid::generate(4)->string;
                 $form['created_by'] = $created_by;
                 PurchaseRecord::create($form);
                 return response()->json(['message' => 'Purchase Record uploaded successfully', 'success' => true], 201);
             }
         } catch (Exception $e) {
             if (isset($e->errorInfo[2])) {
                 return Response()->json(['message' => $e->errorInfo[2], 'error_code' => $e->errorInfo[1], 'success' => false], 500);
             } else {
                 return Response()->json(['message' => $e->getMessage(), 'success' => false], 500);
             }
         }
     }
 
       //get Customer Record 
       public function get(Request $request)
       {
           try {
               $purchase_record_id = $request->input('purchase_record_id');
               if ($purchase_record_id) {
                   $data = PurchaseRecord::where('purchase_record_id', $purchase_record_id)->first();
               } else {
                   $data = PurchaseRecord::orderBy('purchase_record_sno', 'DESC')->get();;
               }
               return response()->json(['data' => $data, 'success' => true], 200);
           } catch (Exception $e) {
               return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
           }
       }
}
