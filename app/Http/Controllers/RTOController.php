<?php

namespace App\Http\Controllers;

use App\Models\RTO;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Webpatser\Uuid\Uuid;

class RTOController extends Controller
{
    //add RTO 
    public function post(Request $request)
    {
        $r_t_o_id = $request->input('r_t_o_id');
        $function = $request->input('function');

        try {
            $request->validate([
                'reg_no'        => 'required',
                'date'          => 'required',
                'customer_name' => 'required',
                'contact_no'    => 'required',
                'model'         => 'required',
                'year'          => 'required'
            ]);
            $reg_no          = $request->get('reg_no');
            $date            = $request->get('date');
            $customer_name   = $request->get('customer_name');
            $contact_no      = $request->get('contact_no');
            $model           = $request->get('model');
            $year            = $request->get('year');
            $transfer_amount = $request->get('transfer_amount');
            $bank_transfer   = $request->get('bank_transfer');
            $self_transfer   = $request->get('self_transfer');
            $rto_agent_name  = $request->get('rto_agent_name');
            $remarks         = $request->get('remarks');
            $created_by      = $request->user()->user_id;
            $form = array(
                'reg_no'          => $reg_no,
                'date'            => $date,
                'customer_name'   => $customer_name,
                'contact_no'      => $contact_no,
                'model'           => $model,
                'year'            => $year,
                'transfer_amount' => $transfer_amount,
                'bank_transfer'   => $bank_transfer,
                'self_transfer'   => $self_transfer,
                'rto_agent_name'  => $rto_agent_name,
                'remarks'         => $remarks,
            );
            if ($function === 'update') {
                RTO::where('r_t_o_id', $r_t_o_id)->update($form);
                return response()->json(['message' => 'RTO Record updated successfully', 'success' => true], 200);
            } else {
                $form['r_t_o_id']   = Uuid::generate(4)->string;
                $form['created_by'] = $created_by;
                RTO::create($form);
                return response()->json(['message' => 'RTO Record uploaded successfully', 'success' => true], 201);
            }
        } catch (Exception $e) {
            if (isset($e->errorInfo[2])) {
                return Response()->json(['message' => $e->errorInfo[2], 'error_code' => $e->errorInfo[1], 'success' => false], 500);
            } else {
                return Response()->json(['message' => $e->getMessage(), 'success' => false], 500);
            }
        }
    }

      //get RTO 
      public function get(Request $request)
      {
          try {
              $r_t_o_id = $request->input('r_t_o_id');
              if ($r_t_o_id) {
                  $data = RTO::where('r_t_o_id', $r_t_o_id)->first();
              } else {
                  $data = RTO::orderBy('r_t_o_sno', 'DESC')->get();;
              }
              return response()->json(['data' => $data, 'success' => true], 200);
          } catch (Exception $e) {
              return response()->json(['message' => $e->getMessage(), 'success' => false], 500);
          }
      }
}
