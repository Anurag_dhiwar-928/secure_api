<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Closure;
use Illuminate\Http\Request;

use App\Models\RolePrmsnPrmsnGrp;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ... $prms)
    {
        $user = Auth::user();

        // \Log::info(array('sssssssssssssssssssss' => $prms));
        
        /**
         * Add this permission in Laravel session while login and
         * access here, so no need to query everytime while routing
         * and delete when logout
         */
        $permissions = RolePrmsnPrmsnGrp::select('permission_perms_grps.permission_name')->leftJoin('permission_perms_grps', 'permission_perms_grps.permission_perms_grp_id', '=', 'role_prmsn_prmsn_grps.permission_perms_grp_id')
        ->where('role_prmsn_prmsn_grps.role_id', $user->role_id)->get()->pluck('permission_name')->toArray();

        // $permissions = $request->session()->get('prms');

        // \Log::info($permissions);

        $isPermitted = 0 == count(array_diff($prms, $permissions));

        if($isPermitted){
            return $next($request);
        } else {
            // return new Response(view('notauthorized')->with('role', 'admin'));
            return new Response(['message' => 'Access Not Granted.'], 401);
        }
    }
}
