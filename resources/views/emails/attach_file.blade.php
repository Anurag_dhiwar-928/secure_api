@component('mail::message')
# Hi,

<p>
{{$data['description']}}
please find attachment ({{$form['file_type']}})
</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
