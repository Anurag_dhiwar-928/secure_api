<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRTOSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_t_o_s', function (Blueprint $table) {
            $table->increments('r_t_o_sno');
            $table->uuid('r_t_o_id')->index();
            $table->string('reg_no', 20);
            $table->date('date');
            $table->string('customer_name', 30);
            $table->string('contact_no', 25);
            $table->string('model', 25);
            $table->string('year', 6);
            $table->unsignedDecimal('transfer_amount', $precision = 12, $scale = 2)->nullable();
            $table->string('bank_transfer')->nullable();
            $table->string('self_transfer')->nullable();
            $table->string('rto_agent_name', 30);
            $table->text('remarks')->nullable();
            $table->boolean('active')->default(1);
            $table->string('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_t_o_s');
    }
}
