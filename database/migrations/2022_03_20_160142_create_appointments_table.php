<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('appointment_sno');
            $table->uuid('appointment_id')->index();
            $table->string('enquiry_no');
            $table->string('name');
            $table->string('mobile');
            $table->string('email')->nullable();
            $table->text('address');
            $table->uuid('car_id')->nullable();
            $table->enum('status',['Pending','Complete'])->default('Pending');
            $table->boolean('is_viewed')->default(0);
            $table->text('remarks')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
