<?php
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Webpatser\Uuid\Uuid;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('user_id')->index();
            $table->string('name');
            $table->string('mobile')->unique();
            $table->string('password');
            $table->tinyInteger('user_type')->default(1)->length(1);
            $table->boolean('delete_status')->default(0);
            $table->rememberToken();
            $table->uuid('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
        });
        $uuid = Uuid::generate(4)->string;
            $form = array (
                'user_id'    => $uuid,
                'name'       => "Admin",
                'mobile'     => "8889436508",
                'password'   => "admin888",
                'user_type'  => 1,
                'created_by' => $uuid
            );
            User::create($form);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
