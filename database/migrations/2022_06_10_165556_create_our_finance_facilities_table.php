<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOurFinanceFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('our_finance_facilities', function (Blueprint $table) {
            $table->increments('our_finance_sno');
            $table->uuid('our_finance_id')->index();
            $table->string('name', 70);
            $table->string('emp_name', 30);
            $table->string('emp_contact', 13);
            $table->string('emp_wa_contact', 13)->nullable();
            $table->string('designation', 30)->nullable();
            $table->string('img_url', 30);
            $table->text('remarks')->nullable();
            $table->boolean('active')->default(1);
            $table->string('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('our_finance_facilities');
    }
}
