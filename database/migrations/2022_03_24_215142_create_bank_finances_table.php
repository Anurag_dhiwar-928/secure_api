<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankFinancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_finances', function (Blueprint $table) {
            $table->increments('bank_finance_sno');
            $table->uuid('bank_finance_id')->index();
            $table->string('reg_no', 20);
            $table->date('date');
            $table->string('customer_name', 30);
            $table->string('contact_no', 25);
            $table->text('address');
            $table->unsignedDecimal('total_amount', $precision = 12, $scale = 2);
            $table->unsignedDecimal('advanced_amount', $precision = 12, $scale = 2);
            $table->unsignedDecimal('down_payment', $precision = 12, $scale = 2);
            $table->unsignedDecimal('finance_amount', $precision = 12, $scale = 2);
            $table->unsignedDecimal('payout', $precision = 12, $scale = 2);
            $table->string('bank_name', 50);
            $table->text('remarks')->nullable();
            $table->boolean('active')->default(1);
            $table->string('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_finances');
    }
}
