<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('car_sno');
            $table->uuid('car_id')->index();
            $table->string('brand');
            $table->string('name');
            $table->string('model');
            $table->string('kms');
            $table->string('owner')->nullable();
            $table->string('insurance')->nullable();
            $table->unsignedDecimal('price', $precision = 12, $scale = 2)->nullable();
            $table->unsignedDecimal('commission', $precision = 12, $scale = 2)->nullable();
            $table->unsignedDecimal('discount', $precision = 12, $scale = 2)->nullable();
            $table->unsignedDecimal('sold_price', $precision = 12, $scale = 2)->nullable();
            $table->string('fuel')->nullable();
            $table->text('owner_comment')->nullable();
            $table->enum('status',['Sold','Sale'])->default('Sale');
            $table->boolean('is_parked')->default(0);
            $table->date('sold_date')->nullable();
            $table->string('location')->nullable();
            $table->string('sold_location')->nullable();
            $table->text('photos')->nullable();
            $table->text('sold_photos')->nullable();
            $table->text('remarks')->nullable();
            $table->uuid('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
