<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSellCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_cars', function (Blueprint $table) {
            $table->increments('sell_car_sno');
            $table->uuid('sell_car_id')->index();
            $table->string('registration_no', 20)->nullable();
            $table->string('brand', 30);
            $table->string('reg_year', 4);
            $table->string('model', 30);
            $table->string('variant', 50);
            $table->string('fuel', 20);
            $table->string('ownership', 20);
            $table->string('km_driven', 20);
            $table->string('sell_price', 20);
            $table->string('user_name', 30);
            $table->string('mobile', 12);
            $table->string('address', 80)->nullable();
            $table->boolean('is_whatsapp')->default(0);
            $table->enum('status',['Sold','Sale','Pending'])->default('Pending');
            $table->text('remarks')->nullable();
            $table->boolean('active')->default(1);
            $table->string('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_cars');
    }
}
