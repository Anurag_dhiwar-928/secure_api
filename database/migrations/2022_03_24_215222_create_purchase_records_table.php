<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_records', function (Blueprint $table) {
            $table->increments('purchase_record_sno');
            $table->uuid('purchase_record_id')->index();
            $table->string('reg_no', 20);
            $table->date('date');
            $table->string('customer_name', 30);
            $table->string('contact_no', 25);
            $table->text('address');
            $table->unsignedDecimal('total_deal', $precision = 12, $scale = 2);
            $table->unsignedDecimal('cash_amount', $precision = 12, $scale = 2)->nullable();
            $table->unsignedDecimal('transfer_amount', $precision = 12, $scale = 2)->nullable();
            $table->unsignedDecimal('bank_finance', $precision = 12, $scale = 2)->nullable();
            $table->string('utr_no', 20)->nullable();
            $table->text('transfer_ac_detail')->nullable();
            $table->string('bank_name', 30)->nullable();
            $table->text('remarks')->nullable();
            $table->boolean('active')->default(1);
            $table->string('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_records');
    }
}
