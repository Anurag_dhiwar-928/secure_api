<?php

namespace Database\Seeders;

use App\Models\AnimalFeedMonitoring;
use App\Models\GateAutomation;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        AnimalFeedMonitoring::factory(20)->create();
        GateAutomation::factory(20)->create();
    }
}
