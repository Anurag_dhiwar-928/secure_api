<?php

use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\BankFinanceController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\CustomerRecordController;
use App\Http\Controllers\OurTeamController;
use App\Http\Controllers\PurchaseRecordController;
use App\Http\Controllers\RTOController;
use App\Http\Controllers\SaleCarController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * user login
 */
Route::post('/user/auth', [UserController::class, 'authentication']); //login 

/**
 * users group
 */
Route::group(
    ['middleware' => 'auth:api'],
    function () {

        Route::put('/v1/users', [UserController::class, 'createUsers']); //create users 

        Route::get('/v1/users', [UserController::class, 'getUsers']); //get all list of users 

        Route::get('/v1/users/{user_id}', [UserController::class, 'getUsersById']); //get all user detail by user id

        Route::patch('/v1/users', [UserController::class, 'updateUsers']); //update users 

        Route::delete('/v1/users/{user_id}', [UserController::class, 'deleteUsers']); //delete users 

        Route::get('/v1/users/login/session', [UserController::class, 'getSessionLogin']); //get all user detail by user id

    }
);

/**
 * begin Website Route
 */

Route::get('/v1/cars', [CarController::class, 'getAll']);

Route::get('/v1/our_team', [OurTeamController::class, 'get']); //get our team 

Route::get('/v1/our_finance', [OurTeamController::class, 'getFinance']); //get our finance facility list 

Route::post('/v1/appointments', [AppointmentController::class, 'create']);

Route::post('/v1/sale_cars', [SaleCarController::class, 'post']);

Route::get('/v1/sale_cars', [SaleCarController::class, 'get']);

Route::get('/v1/banners', [BannerController::class, 'get']);


/**
 * end Website Route
 */

Route::group(['middleware' => 'auth:api'], function () {

    Route::post('/v1/cars', [CarController::class, 'create']);

    Route::get('/v1/appointments', [AppointmentController::class, 'getAll']);

    Route::post('/v1/banners', [BannerController::class, 'post']);
});

/**
 * Official Route
 */
Route::group(
    ['middleware' => 'auth:api'],
    function () {

        Route::post('/v1/rto', [RTOController::class, 'post']); //create/ update RTO 

        Route::get('/v1/rto', [RTOController::class, 'get']); //get rto 

        Route::post('/v1/bank_finance', [BankFinanceController::class, 'post']); //create/ update Bank Finance 

        Route::get('/v1/bank_finance', [BankFinanceController::class, 'get']); //get Bank Finance 

        Route::post('/v1/customer_record', [CustomerRecordController::class, 'post']); //create/ update Customer Record 

        Route::get('/v1/customer_record', [CustomerRecordController::class, 'get']); //get Customer Record 
        
        Route::post('/v1/purchase_record', [PurchaseRecordController::class, 'post']); //create/ update Purchase Record 

        Route::get('/v1/purchase_record', [PurchaseRecordController::class, 'get']); //get Purchase Record 
        
        Route::post('/v1/our_team', [OurTeamController::class, 'post']); //create/ update our team 

        Route::post('/v1/our_finance', [OurTeamController::class, 'postFinance']); //create/ update our fanance facility list 

        Route::get('/v1/dashboard_count', [OurTeamController::class, 'getCountForDashboard']); //get module count for dashboard

    }
);